#ifndef __TF_CLASSICUBE_H
#define __TF_CLASSICUBE_H

#include <curl/curl.h>

#define TF_API_SERVER_URL "https://classicube.net/api/"
#define TF_API(name) (TF_API_SERVER_URL name)

#define CC_MAX_RESULTS 5

typedef enum {
    TF_CC_SUCCESS, // Successful execution
    TF_CC_INIT,    // Initialisation error
    TF_CC_LOGIN,   // Failed to log in
    TF_CC_FILE,    // Unable to read/create file
    TF_CC_CURL,    // cURL error
    TF_CC_JSON,    // JSON parsing error
    TF_CC_AUTHENTICATED, // Already authenticated
    TF_CC_QUERY,
} TFCCStatus;

typedef struct TFCCQuery {
    int listed;
    char name[33];
    char mppass[33];
    char ip[33];
    int  port;
} TFCCQuery;

typedef struct TFClassiCube {
    CURLcode response;
    CURL* session;
    char username[65];
    char cookiejar[100];

    TFCCStatus status;
    TFCCQuery  query[CC_MAX_RESULTS];

    // json buffer
    char* buffer; size_t bufsize;
} TFClassiCube;

//// Functions
int  tf_classicube_init(TFClassiCube* cc);
void tf_classicube_quit(TFClassiCube* cc);
int  tf_classicube_login(TFClassiCube* cc,
                         const char* username, const char* password);
int  tf_classicube_query(TFClassiCube* cc, const char* search);

const char* tf_classicube_strerror(TFClassiCube* cc);

#endif
