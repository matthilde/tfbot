#ifndef __TF_BOT_H
#define __TF_BOT_H

#include <pthread.h>
#include "map.h"
#include "log.h"
#include "schematic/schematic.h"
#include "classicube.h"
#include "lockdown.h"

#define BOT_LOG_SIZE 2048
#define MAX_JOB_THREADS 8

// Types
typedef struct TFPlayer {
    int online;
    short x, y, z; byte pitch, yaw;
    char name[65];
} TFPlayer;

// Contains configuration stuff and other amazing things...
typedef struct TFState {
    // Player ID corresponding to the owner of the bot.
    int owner;
    // is CPE enabled?
    int cpe;

    // Builder stuff
    int buildjobid;
    int builddelay; // in ms
    TFSchematic *bschem;
    int buildx, buildy, buildz;
    int buildstop; // set to 1 to force-stop it.

    // Packet reception stuff
    int ignoremap; // Ignore map changes and download

    // Lockdown stuff :trol:
    int lockdownjobid;
    int lockdownstop;
    TFBlockQueue blockqueue;

    long extra[32];
} TFState;

typedef struct TFClient {
    int master, *slaves, slavecount;

    // Connection information
    char ip[64]; int port;

    // Threading stuff.
    pthread_mutex_t lock;
    pthread_t jobs[MAX_JOB_THREADS];

    TFLog *log, *chat;

    char name[65];
    char clientname[65];
    TFPlayer players[256];

    int authenticated;
    TFClassiCube cc;

    TFMap* map;
    TFState state;
} TFClient;

#define CLIENTLOG(client, ...) ({               \
        tf_job_lock(client); \
        tf_log(client->log, __VA_ARGS__);        \
        tf_job_unlock(client); })

// Functions
void tf_message(int sock, const char* msg);
void tf_teleport(int sock, short x, short y, short z);
void tf_place_block(int sock, short x, short y, short z, int bid);
// Log in for one socket.
int tf_login(const char* ip, int port,
             const char* name, const char* mppass,
             const char* clientname);

// Packet processing
int tf_client_recv(TFClient* client, int* pid, PacketArg* argbuf);

// Threading stuff
int   tf_job_start(TFClient* client, void *(*routine)(void *));
void  tf_job_stop(TFClient* client, int id);
void  tf_job_stop_all(TFClient* client);
void  tf_job_lock(TFClient* client);
void  tf_job_unlock(TFClient* client);

// Client struct abstraction
TFClient* tf_new_client(const char* namefmt, const char* clientname,
                        int slavecount);
void      tf_free_client(TFClient* client);

int tf_client_connect_master(TFClient* client, const char* mppass,
                             const char* ip, int port);
int tf_client_connect_slave(TFClient* client, int id, const char* mppass,
                            const char* ip, int port);
void tf_client_disconnect_master(TFClient* client);
void tf_client_disconnect_slave(TFClient* client, int id);

#endif
