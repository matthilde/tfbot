/*
 * TFBot by matthilde
 *
 * builder.c
 *
 * Bot job that does stuff such as building.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "types.h"
#include "packet.h"
#include "bot.h"
#include "schematic/schematic.h"

#define CHUNK_DIM 4
#define CHUNK_SIZE (CHUNK_DIM*CHUNK_DIM*CHUNK_DIM)

int received_signal = 0;

// 4x4 chunk
typedef struct bchunk {
    byte blocks[CHUNK_SIZE], toplace[CHUNK_SIZE];
    int currentpos;
    // relative offset of the chunk
    int x, y, z;
    int done;
} bchunk;

typedef struct bworker {
    int sock, position, sleeping;
} bworker;

typedef struct builder_state {
    TFClient *client;

    bchunk *chunks;
    int chunkcount;

    bworker* workers;
    int workercount;

    int delayms; // delay in milliseconds
    TFSchematic *schematic;
} builder_state;

//// Z-order space filling curve

/*
static int zcurve_encode(int x, int y, int z) {
    int n = 0, offset = 0;
    for (;x || y || z; offset += 3) {
        n |= (((z&1) << 2) | ((y&1) << 1) | (x&1)) << offset;
        x >>= 1; y >>= 1; z >>= 1;
    }
    return n;
}
*/

static void zcurve_decode(int idx, int *px, int *py, int *pz) {
    int offset = 0;
    int x = 0, y = 0, z = 0;
    for (; idx; idx >>= 3) {
        x |= (idx & 1) << offset;
        y |= ((idx >> 1) & 1) << offset;
        z |= ((idx >> 2) & 1) << offset;
        offset++;
    }
    *px = x; *py = y; *pz = z;
}

//////////////////////////////////////////////////////////////////////

static bchunk* builder_generate_chunks(TFClient *client,
                                       TFSchematic* schem,
                                       int *sz) {
    int size = 32, used = 0, idx = 0;
    bchunk *chunks = (bchunk*)xmalloc(sizeof(bchunk) * size);
    bchunk *curchunk;

    int x = 0, y = 0, z = 0, cx, cy, cz; int anyblock;

    while (x < schem->x || y < schem->y || z < schem->z) {
        zcurve_decode(idx++, &x, &y, &z);
        x <<= 2; y <<= 2; z <<= 2;
        if (x < schem->x && y < schem->y && z < schem->z) {
            ++used;
            if (used >= size)
                chunks = (bchunk*)xrealloc(chunks,
                                           sizeof(bchunk) * (size*=2));
            curchunk = &chunks[used - 1];
            curchunk->x = x;
            curchunk->y = y;
            curchunk->z = z;
            anyblock = 0;
            for (int i = 0; i < CHUNK_SIZE; ++i) {
                cx = x + (i % CHUNK_DIM);
                cy = y + ((i / CHUNK_DIM) / CHUNK_DIM);
                cz = z + ((i / CHUNK_DIM) % CHUNK_DIM);
                int ox = client->state.buildx;
                int oy = client->state.buildy;
                int oz = client->state.buildz;

                curchunk->toplace[i] = 0;
                if (cx < schem->x && cy < schem->y && cz < schem->z) {
                    int schblock = tf_schematic_getblock(schem,
                                                         cx, cy, cz);
                    curchunk->toplace[i] = 1;
                    curchunk->blocks[i]  = schblock;
                    if (client->map->buffer) {
                        int mapblock = tf_map_getblock(client->map,
                                                    ox+cx, oy+cy, oz+cz);
                        curchunk->toplace[i] = mapblock != schblock;
                    }
                }
                anyblock = anyblock || curchunk->toplace[i];
            }

            used -= !anyblock;
        }
    }

    *sz = used;
    return chunks;
}

static inline void builder_change_chunk(builder_state* state,
                                 int workerid, int chunkid) {
    TFClient *client = state->client;
    bworker* worker = &(state->workers[workerid]);
    bchunk* chunk;
    if (chunkid >= state->chunkcount)
        worker->sleeping = 1;
    else {
        chunk = &(state->chunks[chunkid]);
        worker->position = chunkid;
        chunk->currentpos = 0;
        worker->sleeping = 0;

        if (worker->sock == client->master) tf_job_lock(client);
        tf_send(worker->sock, 0x8, 0xff,
                ((chunk->x + client->state.buildx) << 5) + 64,
                ((chunk->y + client->state.buildy) << 5) + 64,
                ((chunk->z + client->state.buildz) << 5) + 64,
                0, 0);
        if (worker->sock == client->master) tf_job_unlock(client);
    }
}

static void builder_work(builder_state* state, int i) {
    TFClient* client = state->client;
    bworker* worker = &(state->workers[i]);
    if (worker->sleeping) return;
    
    bchunk* chunk   = &(state->chunks[worker->position]);
    int curpos;

    // Find next to place
    for (;   chunk->currentpos < CHUNK_SIZE &&
             !(chunk->toplace[chunk->currentpos])
             ; chunk->currentpos++);

    curpos = chunk->currentpos;
    if (curpos < CHUNK_SIZE) {
        int x = chunk->x + (curpos % CHUNK_DIM);
        int y = chunk->y + ((curpos / CHUNK_DIM) / CHUNK_DIM);
        int z = chunk->z + ((curpos / CHUNK_DIM) % CHUNK_DIM);
        x += client->state.buildx;
        y += client->state.buildy;
        z += client->state.buildz;

        if (worker->sock == client->master) tf_job_lock(client);
        // tf_place_block(worker->sock, x, y, z, chunk->blocks[curpos]);
        tf_send(worker->sock, 0x5, x, y, z, 1, chunk->blocks[curpos]);
        if (worker->sock == client->master) tf_job_unlock(client);

        chunk->currentpos++;
    } else {
        builder_change_chunk(state, i,
                             worker->position + state->workercount);
    }
}

static void builder_build(builder_state* state) {
    TFClient *client = state->client;
    struct timespec delay = {
        state->delayms / 1000,
        (state->delayms % 1000) * 1000000
    };
        
    int stillawake = 1;
    if (state->chunkcount <= 0) return;
    
    for (int i = 0; i < state->workercount; ++i) {
        builder_change_chunk(state, i, i);
    }

    while (stillawake && !(client->state.buildstop)) {
        nanosleep(&delay, NULL);
        stillawake = 0;
        for (int i = 0; i < state->workercount; ++i) {
            builder_work(state, i);
            stillawake = stillawake || !(state->workers[i].sleeping);
        }
    }

    if (client->state.buildstop)
        CLIENTLOG(client, "Forced build interruption.\n");
}

void tf_build_schematic(TFClient* client, TFSchematic* schem,
                        int includemaster) {
    builder_state state;
    state.client = client;
    state.chunks = builder_generate_chunks(client, schem,
                                           &(state.chunkcount));
    state.workers = (bworker*)xmalloc(sizeof(bworker) *
                                  (client->slavecount + (includemaster!=0)));
    int used = 0;
    for (int i = 0; i < client->slavecount; ++i) {
        if (client->slaves[i] != -1) {
            state.workers[used].sock = client->slaves[i];
            ++used;
        }
    }
    if (includemaster)
        state.workers[used++].sock = client->master;

    state.workercount = used;
    state.delayms     = client->state.builddelay;

    builder_build(&state);

    free(state.workers);
    free(state.chunks);
}

void* tf_build_job(void *c) {
    TFClient* client = (TFClient*)c;

    tf_job_lock(client);
    // tf_message(client->master, "ahhhh i'm building :weary:");
    tf_log(client->log, "Began building something.\n");
    tf_job_unlock(client);

    tf_build_schematic(client, client->state.bschem, 1);
    tf_job_lock(client);
    tf_free_schematic(client->state.bschem);
    client->state.buildstop = 0;
    client->state.bschem = NULL;
    client->state.buildjobid = -1;

    tf_log(client->log, "Finished building.\n");
    // tf_message(client->master, "me and the bois are done.");
    tf_job_unlock(client);

    client->jobs[client->state.buildjobid] = 0;
    client->state.buildjobid = -1;

    return NULL;
}
