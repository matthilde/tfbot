/*
 * TFBot by matthilde
 *
 * log.c
 *
 * Logging facilities.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <sys/types.h>
#include <string.h>

#include "types.h"
#include "log.h"

TFLog* tf_new_log(size_t buffersize) {
    TFLog* log = (TFLog*)malloc(sizeof(TFLog));
    if (!log) die("tf_new_log: malloc");

    log->buffer     = (char*)malloc(sizeof(char) * buffersize);
    log->buffersize = buffersize;
    log->used       = 0;

    return log;
}

void tf_free_log(TFLog* log) {
    free(log->buffer); free(log);
}

static char* allocate_buffer_space(TFLog *log, size_t size) {
    if (size >= log->buffersize) return NULL;
    size_t freespace = log->buffersize - log->used;

    char* result;
    if (size <= freespace) {
        result = &(log->buffer[log->used]);
        log->used += size;

        return result;
    } else {
        size_t excess = size - freespace;
        // shift to the left...

        result = &(log->buffer[log->buffersize - size]);
        memmove(log->buffer, &(log->buffer[excess]),
                log->used - excess);
        log->used = log->buffersize;

        return result;
    }
}

static ssize_t get_format_needed_size(const char* fmt, va_list args) {
    ssize_t result = vsnprintf(NULL, 0, fmt, args);
    return result;
}

void tf_log(TFLog* log, const char* fmt, ...) {
    va_list va, vb;

    va_start(va, fmt);
    va_copy(vb, va);
    ssize_t size = get_format_needed_size(fmt, vb);
    va_end(vb);
    char* buf = allocate_buffer_space(log, size);
    vsprintf(buf, fmt, va);
    va_end(va);
}

void tf_log_print_buffer(TFLog* log, ssize_t maxlines) {
    if (maxlines == -1) {
        fwrite(log->buffer, 1, log->used, stdout);
        putchar('\n');
    } else {
        ssize_t newlines = maxlines; int i = log->used - 1;
        i -= log->buffer[i] == '\n';
        for (; newlines > 0 && i >= 0; --i)
            if (log->buffer[i] == '\n') --newlines;
        ++i;
        fwrite(log->buffer + i, 1, log->used - i, stdout);
        putchar('\n');
    }
}
