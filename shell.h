#ifndef __TF_SHELL_H
#define __TF_SHELL_H

#include "bot.h"

typedef int (*CommandFn)(TFClient*, char**, int);
struct cmdlistRow { const char* cmd; int argcount; CommandFn fn; };

//// Functions
void tf_shell_init();
int  tf_shell_execute(TFClient* client, char* cmd, int silent);
int  tf_shell_execute_script(TFClient* client, char* filename);

#endif
