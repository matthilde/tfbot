/*
 * TFBot by matthilde
 *
 * shell.c
 *
 * Command-line shell for the bot so I can do stuff with it.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/wait.h>

#include "bot.h"
#include "types.h"
// #include "commands.h"
#include "shell.h"

#include "schematic/schematic.h"
#include "schematic/image.h"
#include "builder.h"
#include "classicube.h"
#include "lockdown.h"

//////////////////////////////////////////////////////////////////////
// COMMANDS

static int cmd_comment(TFClient* client, char** argv, int argc) {
    (void)client; (void)argc; (void)argv;
    return 0;
}

static int cmd_argdump(TFClient* client, char** argv, int argc) {
    (void)client;
    for (int i = 0; i < argc; ++i) {
        puts(argv[i]);
    }
    return 0;
}

static int cmd_listplayers(TFClient* client, char** argv, int argc) {
    (void)argv; (void)argc;
    TFPlayer* players = client->players;
    for (int i = 0;i < 256; ++i) {
        if (players[i].online)
            printf("%3d\t(%3d %3d %3d)\t%s\n",
                   i,
                   players[i].x>>5, players[i].y>>5, players[i].z>>5,
                   players[i].name);
    }

    return 0;
}

static int cmd_say(TFClient* client, char** argv, int argc) {
    (void)argc;
    tf_message(client->master, argv[0]);

    return 0;
}

static int cmd_sayall(TFClient* client, char** argv, int argc) {
    (void)argc;

    tf_message(client->master, argv[0]);
    for (int i = 0; i < client->slavecount; ++i)
        if (client->slaves[i] != -1)
            tf_message(client->slaves[i], argv[0]);

    return 0;
}

static int cmd_setowner(TFClient* client, char** argv, int argc) {
    (void)argc;
    int pid = atoi(argv[0]);
    if (!(client->players[pid].online)) return 1;
    fprintf(stderr, "The owner is now %s!\n",
            client->players[pid].name);
    client->state.owner = pid;
    return 0;
}

// sphere radius:bid
static int cmd_sphere(TFClient* client, char** argv, int argc) {
    (void)argc;
    
    int rad = atoi(argv[0]);
    int bid = atoi(argv[1]);

    TFPlayer *owner = &(client->players[client->state.owner]);
    int ox = owner->x >> 5;
    int oy = owner->y >> 5;
    int oz = owner->z >> 5;

    for (int x = -rad - 2; x <= rad + 2; ++x)
        for (int y = -rad - 2; y <= rad + 2; ++y)
            for (int z = -rad - 2; z <= rad + 2; ++z) {
                if (rad*rad > x*x + y*y + z*z)
                    tf_place_block(client->master, x+ox, y+oy, z+oz, bid);
            }
    return 0;
}

static int cmd_load(TFClient* client, char** argv, int argc) {
    (void)argc;

    if (client->state.buildjobid != -1) return 1;
    if (client->state.bschem != NULL) return 1;
    
    TFSchematic* schematic = tf_read_schematic(argv[0]);
    if (!schematic) {
        fprintf(stderr, "Unable to load schematic.\n");
        return 1;
    }

    client->state.bschem = schematic;

    return 0;
}

static int cmd_loadimg(TFClient* client, char** argv, int argc) {
    (void)argc;

    if (client->state.buildjobid != -1) return 1;
    if (client->state.bschem != NULL) return 1;

    TFSchematic* schematic = tf_schematic_from_image(argv[0]);
    if (!schematic) {
        fprintf(stderr, "Unable to load image.\n");
        return 1;
    }

    client->state.bschem = schematic;

    return 0;
}

static int cmd_buildat(TFClient* client, char** argv, int argc) {
    (void)argc;

    if (client->state.buildjobid != -1) return 1;
    if (client->state.bschem == NULL) return 1;

    client->state.buildx = atoi(argv[0]);
    client->state.buildy = atoi(argv[1]);
    client->state.buildz = atoi(argv[2]);

    fprintf(stderr, "Building...\n");
    client->state.buildjobid = tf_job_start(client, tf_build_job);

    return 0;
}

static int cmd_build(TFClient* client, char** argv, int argc) {
    (void)argc; (void)argv;

    if (client->state.buildjobid != -1) return 1;
    if (client->state.bschem == NULL) return 1;
    
    TFPlayer *owner = &(client->players[client->state.owner]);
    client->state.buildx = owner->x >> 5;
    client->state.buildy = owner->y >> 5;
    client->state.buildz = owner->z >> 5;

    fprintf(stderr, "Building...\n");
    client->state.buildjobid = tf_job_start(client, tf_build_job);

    return 0;
}

static int cmd_stopbuild(TFClient* client, char **argv, int argc) {
    (void)argc; (void)argv;
    if (client->state.buildjobid != -1) {
        client->state.buildstop = 1;
    }

    return 0;
}

static int cmd_connect(TFClient* client, char **argv, int argc) {
    (void)argc;
    int s = tf_client_connect_master(client, argv[2],
                                     argv[0], atoi(argv[1]));
    return s == -1;
}

static int cmd_conslaves(TFClient* client, char **argv, int argc) {
    (void)argc;
    if (client->master == -1) return 1;

    for (int i = 0; i < client->slavecount; ++i) {
        sleep(1);
        tf_client_connect_slave(client, i, argv[0],
                                client->ip, client->port);
    }
    return 0;
}

static int cmd_viewlog(TFClient* client, char **argv, int argc) {
    (void)argc;
    TFLog *log;
    switch (argv[0][0]) {
    case 'l': log = client->log; break;
    case 'c': log = client->chat; break;
    default: return 1;
    }
    tf_log_print_buffer(log, 10);

    return 0;
}

static int cmd_pipe(TFClient* client, char **argv, int argc) {
    (void)argc; (void)argv;

    int pipes[2];
    if (pipe(pipes) == -1)
        return 1;

    pid_t pid = fork();
    if (pid == 0) {
        if (dup2(pipes[1], 1) == -1) return 1;
        char* const exeargv[] = { "./misc/markov.py", NULL };
        if (execve(exeargv[0], exeargv, NULL) == -1) return 1;
        exit(0);
    } else if (pid == -1)
        return 1;
    else {
        int status;
        while (wait(&status) != pid) sleep(1);
        if (!WIFEXITED(status)) return 1;

        char buffer[65];
        size_t sz = read(pipes[0], buffer, 64);
        buffer[sz] = 0;
        tf_message(client->master, buffer);
    }

    close(pipes[0]); close(pipes[1]);
    return 0;
}

#define CFG_MATCH(s) (strcmp(argv[0], s) == 0)
static int cmd_setcfg(TFClient* client, char** argv, int argc) {
    (void)argc;

    int value = atoi(argv[1]);

    if (CFG_MATCH("owner"))
        client->state.owner = value;
    else if (CFG_MATCH("builddelay"))
        client->state.builddelay = value;
    else if (CFG_MATCH("slavecount"))
        client->slavecount = value;
    else if (CFG_MATCH("name"))
        strcpy(client->name, argv[1]);
    else if (CFG_MATCH("clientname"))
        strcpy(client->clientname, argv[1]);
    else if (CFG_MATCH("ignoremap"))
        client->state.ignoremap = value;
    else
        return 1;

    return 0;
}

static int cmd_cc_login(TFClient* client, char** argv, int argc) {
    (void)argc;
    if (client->authenticated) return 1;

    if (!tf_classicube_init(&(client->cc))) {
        tf_log(client->log, "CC auth init failed! %s\n",
               tf_classicube_strerror(&(client->cc)));
        return 1;
    }
    tf_log(client->log, "Logging in...\n");
    if (!tf_classicube_login(&(client->cc), argv[0], argv[1])) {
        tf_log(client->log, "Log in failed! %s\n",
               tf_classicube_strerror(&(client->cc)));
        return 1;
    }

    tf_log(client->log, "Logged in as %s\n", client->cc.username);
    strcpy(client->name, client->cc.username);

    client->authenticated = 1;
    return 0;
}
static int cmd_cc_logoff(TFClient* client, char** argv, int argc) {
    (void)argv; (void)argc;
    if (!(client->authenticated)) return 1;

    tf_log(client->log, "Logged off!\n");
    client->authenticated = 0;
    return 0;
}
static int cmd_cc_query(TFClient* client, char** argv, int argc) {
    (void)argc;
    if (!(client->authenticated)) return 1;
    char silent = argv[2][0];

    if (!tf_classicube_query(&(client->cc), argv[0])) {
        tf_log(client->log, "Search failed! %s\n",
               tf_classicube_strerror(&(client->cc)));
        return 1;
    }

    if (silent != 't') {
        TFCCQuery *q;
        for (int i = 0; i < CC_MAX_RESULTS; ++i) {
            if (client->cc.query[i].listed) {
                q = &(client->cc.query[i]);
                printf("%d\t%32s\t%32s\t%20s\t%d\n",
                    i + 1, q->mppass, q->name, q->ip, q->port);
            }
        }
    }
    return 0;
}
static int cmd_cc_join(TFClient* client, char** argv, int argc) {
    (void)argc;
    if (!(client->authenticated)) return 1;

    int id = atoi(argv[0]);
    if (id < 1 || id > CC_MAX_RESULTS) return 1;
    if (!(client->cc.query[id - 1].listed)) return 1;
    
    TFCCQuery *q = &(client->cc.query[id - 1]);
    return tf_client_connect_master(client, q->mppass, q->ip, q->port) == -1;
}

static int cmd_lockdown_start(TFClient* client, char** argv, int argc) {
    (void)argv; (void)argc;

    if (client->state.lockdownjobid >= 0) return 1;
    tf_log(client->log, "Lockdown on!\n");

    client->state.ignoremap = 1;
    client->state.blockqueue.tail = NULL;
    client->state.blockqueue.head = NULL;
    client->state.lockdownstop = 0;
    client->state.lockdownjobid = tf_job_start(client, tf_lockdown_job);

    return 0;
}

static int cmd_lockdown_stop(TFClient* client, char** argv, int argc) {
    (void)argc; (void)argv;

    if (client->state.lockdownjobid == -1) return 1;
    tf_log(client->log, "Ending lockdown!\n");
    client->state.ignoremap = 0;
    client->state.lockdownstop = 1;

    return 0;
}

//////////////////////////////////////////////////////////////////////


#define COMMANDS_LENGTH 23
static struct cmdlistRow commands[] = {
    { "#", -1, cmd_comment },
    { "argdump", -1, cmd_argdump },
    { "players", 0, cmd_listplayers },
    { "c", 1, cmd_say },
    { "scream", 1, cmd_sayall },
    { "set-owner", 1, cmd_setowner },
    { "sphere", 2, cmd_sphere },
    { "load", 1, cmd_load },
    { "load-image", 1, cmd_loadimg },
    { "build", 0, cmd_build },
    { "build-at", 3, cmd_buildat },
    { "stop-build", 0, cmd_stopbuild },
    { "connect", 3, cmd_connect },
    { "connect-slaves", 1, cmd_conslaves },
    { "viewlog", 1, cmd_viewlog },
    { "funny", 0, cmd_pipe },
    { "set", 2, cmd_setcfg },
    { "cc-login", 2, cmd_cc_login },
    { "cc-logoff", 0, cmd_cc_logoff },
    { "cc-query", 2, cmd_cc_query },
    { "cc-join",  1, cmd_cc_join },
    { "lockdown-on", 0, cmd_lockdown_start },
    { "lockdown-off", 0, cmd_lockdown_stop },
};

void tf_shell_init() {
    fprintf(stderr, "shell is up.\n");
}

int tf_shell_execute(TFClient* client, char* cmd, int silent) {
    (void)silent;
    char* args[16];
    char *command, *tok;

    command = strtok(cmd, " ");
    if (!command) return 0;
    
    tok = strtok(NULL, ":");
    int argc;
    for (argc = 0; argc < 16 && tok; ++argc) {
        args[argc] = tok;
        tok = strtok(NULL, ":");
    }

    int status = 1;
    for (int i = 0; i < COMMANDS_LENGTH; ++i) {
        if (strcmp(command, commands[i].cmd) == 0) {
            if (commands[i].argcount == -1 ||
                argc == commands[i].argcount)
                status = commands[i].fn(client, args, argc);
            else {
                fprintf(stderr, "Invalid argument count\n");
                status = 1;
            }

            break;
        }
    }

    return status;
}

int tf_shell_execute_script(TFClient* client, char* filename) {
    char buf[256];
    FILE *f = fopen(filename, "r");
    if (!f) return 0;

    int line = 1;
    while (fgets(buf, 256, f) != NULL) {
        size_t slen = strlen(buf);
        if (buf[slen - 1] == '\n') buf[slen - 1] = 0;
        int status = tf_shell_execute(client, buf, 1);
        if (status) {
            fprintf(stderr, "Execution of '%s' failed at line %d\n",
                    filename, line);
            return 0;
        }
        line++;
    }

    return 1;
}
