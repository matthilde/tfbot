#ifndef __TF_BUILDER_H
#define __TF_BUILDER_H

void tf_build_schematic(TFClient* client, TFSchematic* schem,
                        int includemaster);
void *tf_build_job(void *c);

#endif
