#ifndef __TF_LOG_H
#define __TF_LOG_H

#include <stddef.h>
#include <sys/types.h>

typedef struct TFLog {
    char* buffer;
    size_t buffersize, used;
} TFLog;

//// Functions
TFLog* tf_new_log(size_t buffersize);
void   tf_free_log(TFLog* log);
void   tf_log(TFLog* log, const char* fmt, ...);
void   tf_log_print_buffer(TFLog* log, ssize_t maxlines);

#endif
