 _____ _____ ____        _   
|_   _|  ___| __ )  ___ | |_ 
  | | | |_  |  _ \ / _ \| __| Minecraft Classic client
  | | |  _| | |_) | (_) | |_  by matthilde
  |_| |_|   |____/ \___/ \__|

TFBot (or TurboFucker) is a Minecraft Classic client I am currently working on.
The main difference between this client and pyclassic is
 1. It's written in C
 2. It's not a library

FEATURES
--------

There's no specific roadmap for this bot. I add features that I want
progressively. It contains quite a few features so far which makes it a
quite complete client.

 - Basic CPE (Classic Protocol Extension) (packets 0x10 and 0x11)
   NOTE: I implement more in the future.
 - Keeps track of map changes, player list and player movement
 - Command-line shell (see SHELLDOCS.txt)
 - Basic multibot (called "slaves") support.
 - Autobuild system which supports ClassiWorld and Schematic
 - ClassiCube authentication
 - Commands only I can use
 - Keeps track of chat, can even send messages! (wow!!)
 - Basic configuration format based on the command-line shell

DEPENDENCIES
------------

 - zlib
 - editline
 - libcurl
 - json-c

BUILDING
--------

Once you have installed the dependencies based on the Linux distribution that
you have, you just need to type `make`.

WHAT ABOUT SCRIPT KIDDIES?
--------------------------

Just ban them from your server if they cause trouble.
