turbofucker: *.c schematic/*.c
	cc -std=gnu99 -Wall -Wextra *.c schematic/*.c -lm -leditline -lpthread -lz -lcurl -ljson-c -g -o turbofucker

valgrind: turbofucker
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./turbofucker 127.0.0.1 25565
