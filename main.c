/*
 * TFBot by matthilde
 *
 * main.c
 *
 * This file contains the main program.
 */

/*
 * [x] Packet recv/send
 * [x] Keep track of players and map changes
 * [x] Map downloading
 * [x] Logging facilities
 * [x] Build bot
 * [x] Command-line shell
 * [ ] ClassiCube auth
 * [ ] Bot reconnection
 * [ ] CPE extensions
 *   [ ] LongerMessages
 */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <math.h>
#include <zlib.h>
#include <readline/readline.h>
#include <readline/history.h>

#include <pthread.h>

#include "types.h"
#include "packet.h"
#include "bot.h"
#include "log.h"
#include "shell.h"

#define BOTCOUNT 8

TFClient* client;

//////////////////////////////////////////////////////////////////////

void *circle_job(void* c) {
    TFClient *client = c;
    TFPlayer* player; short x, y, z;
    for (;;) {
        sleep(1);
        if (client->state.owner != -1) {
            player = &(client->players[client->state.owner]);
            x = player->x + ((rand()%240) - 120);
            y = player->y + ((rand()%240) - 120);
            z = player->z + ((rand()%240) - 120);

            tf_job_lock(client);
            tf_teleport(client->master, x, y, z);
            tf_job_unlock(client);
        }
    }
    
    return NULL;
}

//////////////////////////////////////////////////////////////////////

pthread_t thread_pid;
int exiting = 0;

void *commandline_shell(void *arg) {
    (void)arg;
    tf_shell_init();
    int status = tf_shell_execute_script(client, "turbofucker.cfg");
    if (!status)
        puts("boot script failed");

    while (!exiting) {
        char* cmd = readline("tfbot> ");
        add_history(cmd);

        if (strcmp(cmd, "q") == 0) {
            exiting = 1;
            return NULL;
        }

        tf_job_lock(client);
        if (tf_shell_execute(client, cmd, 0))
            puts("failure");

        tf_job_unlock(client);
        
        free(cmd);
    }

    return NULL;
}

//////////////////////////////////////////////////////////////////////

// int bots[BOTCOUNT];

void sigint_handler(int a) {
    (void)a;
    exit(0);
}

void bot_atexit() {
    puts("Bye!");

    tf_client_disconnect_master(client);
    tf_free_client(client);
}

void sigpipe_handle(int a) {
    (void)a;
    puts("BOT DISCONNECTED");
}

//////////////////////////////////////////////////////////////////////

static void greetings() {
    /*
    fprintf(stderr, 
    " _____           _           _____           _             \n"
    "|_   _|   _ _ __| |__   ___ |  ___|   _  ___| | _____ _ __ \n"
    "  | || | | | '__| '_ \\ / _ \\| |_ | | | |/ __| |/ / _ \\ '__|\n"
    "  | || |_| | |  | |_) | (_) |  _|| |_| | (__|   <  __/ |   \n"
    "  |_| \\__,_|_|  |_.__/ \\___/|_|   \\__,_|\\___|_|\\_\\___|_|   \n"
        );
    */
    fprintf(stderr,
            "TurboFucker - Minecraft Classic bot by matthilde.\n");
}

int main(int argc, char** argv) {
    (void)argc; (void)argv;
    greetings();

    int error;
    PacketArg argbuf[64];

    // Makes sure to free everything at exit.
    atexit(bot_atexit);
    signal(SIGINT, sigint_handler);
    signal(SIGPIPE, sigpipe_handle);

    client = tf_new_client("tfbot", "TFBot-devbuild", 48);
    client->state.builddelay = 30;
    client->state.owner = -1;
    
    error = pthread_create(&thread_pid, NULL, commandline_shell, NULL);
    if (error) pdie("pthread_create");

    // int id = tf_job_start(client, circle_job);
    // printf("Started circle_job. ID = %d\n", id);

    int pid;
    short x, y, z; byte bid, obid;
    while (!exiting && client->master == -1) sleep(1);
	while (!exiting) {
        int success = tf_client_recv(client, &pid, argbuf);
        if (success) {
            // ...
            if (pid == 0x6 &&
                client->state.lockdownjobid != -1 &&
                !(client->state.lockdownstop)) {
                x   = argbuf[0].c.s;
                y   = argbuf[1].c.s;
                z   = argbuf[2].c.s;
                bid = argbuf[3].c.b;
                obid = tf_map_getblock(client->map, x, y, z);
                if (obid != bid) {
                    tf_job_lock(client);
                    tf_lockdown_queue_add(&(client->state.blockqueue),
                                          x, y, z, obid);
                    tf_job_unlock(client);
                }
            }
        }
	}
	return 0;
}
