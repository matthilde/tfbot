/*
 * TFbot by matthilde
 *
 * classicube.c
 *
 * C wrapper of the ClassiCube API for authentication and server
 * searching/connection.
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <curl/curl.h>
#include <json-c/json.h>

#include "types.h"
#include "classicube.h"

#define CC_FAIL(errcode) ({ cc->status = errcode; return 0; })
#define CC_SUCCESS()     ({ cc->status = TF_CC_SUCCESS; return 1; })
#define CURLCHECK(cc)  ({ if (cc->response != CURLE_OK) CC_FAIL(TF_CC_CURL); })
#define JSONGET(root,memb,ptr) ({ \
            status = json_object_object_get_ex(root, memb, &ptr); \
            if (!status) CC_FAIL(TF_CC_JSON); })

static const char *cc_status_errors[] = {
    "Success.",
    "Initialization has failed.",
    "Log in failed",
    "File error",
    "cURL error",
    "Failed to parse JSON data.",
    "User is already authenticated.",
    "No result."
};

const char* tf_classicube_strerror(TFClassiCube* cc) {
    return cc_status_errors[cc->status];
}

static size_t curl_callback(void* contents,
                            size_t size, size_t nmemb, void* ptr) {
    TFClassiCube* cc = (TFClassiCube*)ptr;
    size_t sz = size * nmemb;

    cc->buffer = (char*)xrealloc(cc->buffer,
                                 sizeof(char) * (cc->bufsize + sz + 1));

    memcpy(&(cc->buffer[cc->bufsize]), (char *)contents, sz);
    cc->bufsize += sz;
    cc->buffer[cc->bufsize] = 0;

    return sz;
}

static CURLcode cc_do_request(TFClassiCube* cc, const char* url,
                              const char* postdata) {
    CURL* curl = cc->session;

    cc->buffer = (char*)realloc(cc->buffer, 1);
    cc->bufsize = 0;
    cc->buffer[0] = 0;

    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl-agent/1.0");
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(curl, CURLOPT_POSTREDIR, CURL_REDIR_POST_ALL);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)cc);

    curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cc->cookiejar);
    curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cc->cookiejar);

    if (postdata) {
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postdata);
    } else
        curl_easy_setopt(curl, CURLOPT_POST, 0);

    return curl_easy_perform(curl);
}

// 0 if fail
int tf_classicube_login(TFClassiCube* cc,
                        const char* username, const char* password) {
    json_object *root, *tmp; char *token, *postreq;
    int status;

    //// FIRST REQUEST: GET TOKEN FOR LOGIN
    // Get first token
    cc->response = cc_do_request(cc, TF_API("login"), NULL);
    CURLCHECK(cc);

    // Parse the data
    root = json_tokener_parse(cc->buffer);
    if (!root) CC_FAIL(TF_CC_JSON);

    // Check if user has already been authenticated.
    JSONGET(root, "authenticated", tmp);
    if (json_object_get_boolean(tmp))
        CC_FAIL(TF_CC_AUTHENTICATED);

    // Retrieve token
    JSONGET(root, "token", tmp);
    token = strdup(json_object_get_string(tmp));
    json_object_put(root);

    //// SECOND REQUEST: LOGIN
    asprintf(&postreq, "username=%s&password=%s&token=%s",
             username, password, token);
    cc->response = cc_do_request(cc, TF_API("login"), postreq);
    CURLCHECK(cc);

    root = json_tokener_parse(cc->buffer);
    if (!root) CC_FAIL(TF_CC_JSON);

    // If it isn't set as authenticated, that means it failed.
    JSONGET(root, "authenticated", tmp);
    if (!json_object_get_boolean(tmp))
        CC_FAIL(TF_CC_LOGIN);

    JSONGET(root, "username", tmp);
    strcpy(cc->username, json_object_get_string(tmp));
    
    free(postreq);
    free(token);
    CC_SUCCESS();
}

// 0 if fail
static int cc_contains(const char* substr, const char* s) {
    size_t substrlen = strlen(substr);
    for (const char* p = s; *p; ++p) {
        if (strncmp(p, substr, substrlen) == 0)
            return 1;
    }
    return 0;
}
int tf_classicube_query(TFClassiCube* cc, const char* search) {
    json_object *root, *serverlist, *row, *tmp;
    TFCCQuery *query;
    int listlen, nthresult = 0;
    int status;

    cc->response = cc_do_request(cc, TF_API("servers"), NULL);
    CURLCHECK(cc);

    root = json_tokener_parse(cc->buffer);
    if (!root) CC_FAIL(TF_CC_JSON);
    JSONGET(root, "servers", serverlist);
    listlen = json_object_array_length(serverlist);

    for (int i = 0; i < CC_MAX_RESULTS; ++i)
        cc->query[i].listed = 0;
    for (int i = 0; i < listlen; ++i) {
        row = json_object_array_get_idx(serverlist, i);
        if (!row) CC_FAIL(TF_CC_JSON);

        JSONGET(row, "name", tmp);
        if (cc_contains(search, json_object_get_string(tmp))) {
            query = &(cc->query[nthresult]);

            query->listed = 1;
            // Server name
            strncpy(query->name, json_object_get_string(tmp), 32);
            query->name[32] = 0;
            // User mppass
            JSONGET(row, "mppass", tmp);
            memcpy(query->mppass, json_object_get_string(tmp), 32);
            query->mppass[32] = 0;
            // IP address
            JSONGET(row, "ip", tmp);
            strncpy(query->ip, json_object_get_string(tmp), 32);
            query->ip[32] = 0;
            // Port
            JSONGET(row, "port", tmp);
            query->port = json_object_get_int(tmp);

            if (++nthresult == CC_MAX_RESULTS) break;
        }
    }

    return 1;
}

// 0 if fail
int tf_classicube_init(TFClassiCube* cc) {
    struct curl_slist *headers = NULL;

    curl_global_init(CURL_GLOBAL_ALL);
    cc->session = curl_easy_init();
    if (!(cc->session)) {
        curl_global_cleanup();
        CC_FAIL(TF_CC_INIT);
    }
    cc->buffer = (char*)xmalloc(sizeof(char) * 1);
    sprintf(cc->cookiejar, "/tmp/cc-cookie-%ld.txt", time(NULL));

    headers = curl_slist_append(headers, "Accept: application/json");

    curl_easy_setopt(cc->session, CURLOPT_HTTPHEADER, headers);

    CC_SUCCESS();
}

void tf_classicube_quit(TFClassiCube* cc) {
    remove(cc->cookiejar);
    free(cc->buffer);
    curl_easy_cleanup(cc->session);
    curl_global_cleanup();
}

#ifdef __CC_TEST

int main() {
    TFClassiCube cc; char user[64]; char pass[128];

    tf_classicube_init(&cc);
    puts("Username"); fgets(user, 64, stdin);
    user[strlen(user) - 1] = 0;
    puts("Password"); fgets(pass, 128, stdin);
    pass[strlen(pass) - 1] = 0;

    puts("\nLogging in...");
    if (!tf_classicube_login(&cc, user, pass)) {
        printf("ERROR! %s\n", tf_classicube_strerror(&cc));
        if (cc.status != TF_CC_AUTHENTICATED) {
            puts(cc.buffer);
            goto _cc_test_main_end;
        }
    } else
        printf("Logged in as %s!\n", cc.username);

    for (;;) {
        fgets(pass, 128, stdin);
        pass[strlen(pass) - 1] = 0;

        puts("Searching...");
        if (tf_classicube_query(&cc, pass)) {
            for (int i = 0; i < CC_MAX_RESULTS; ++i) {
                if (cc.query[i].listed) {
                    printf("%d %s %s %s\n",
                           i+1,
                           cc.query[i].mppass,
                           cc.query[i].ip,
                           cc.query[i].name);
                }
            }
        } else {
            printf("ERROR! %s\n", tf_classicube_strerror(&cc));
            puts(cc.buffer);
        }
    }

_cc_test_main_end:
    tf_classicube_quit(&cc);
    return 0;
}

#endif
