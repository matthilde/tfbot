#ifndef __TF_TYPES_H
#define __TF_TYPES_H

#include <stdint.h>
#include <stdlib.h>

#define die(x) { printf("ERROR! " x "\n");      \
                      exit(1); }
#define pdie(x) { perror(x); exit(1); }

static __attribute__((unused)) void* xmalloc(size_t size) {
    void *ptr = malloc(size);
    if (!ptr) pdie("malloc");
    return ptr;
}
static __attribute__((unused)) void* xrealloc(void *oldptr, size_t size) {
    void *ptr = realloc(oldptr, size);
    if (!ptr) pdie("realloc");
    return ptr;
}

typedef uint8_t byte;
typedef int8_t  sbyte;

typedef enum {
    TF_BYTE, TF_SBYTE, TF_SHORT, TF_STRING, TF_BYTE_ARRAY,
    TF_INT, TF_BYTE_ARRAY256,
    TF_END
} PACKET_ARG_TYPES;
#define TF_FLOAT TF_INT

// Used for reception
typedef struct {
    PACKET_ARG_TYPES type;
    union {
        byte  b;
        sbyte sb;
        short s;
        int   i;
        float f;
        char  str[65];
        byte  arr[1024];
    } c;
} PacketArg;

#define IGNORE_LIMIT 0x12
#define PID_SIZE 0x36

#endif
