#ifndef __TF_MAP_H
#define __TF_MAP_H

#include <stddef.h>
#include "types.h"

typedef struct {
    FILE *tempfile; int downloading;
    char tmpfn[64];

    short x, y, z;
    byte* buffer; size_t buflen;
} TFMap;

//// Functions
TFMap* tf_new_map();
void   tf_free_map(TFMap* map);
void   tf_map_dlbegin(TFMap* map);
void   tf_map_dlchunk(TFMap* map, byte* chunk, size_t size);
void   tf_map_dlfinish(TFMap* map, short x, short y, short z);

byte   tf_map_getblock(TFMap* map, short x, short y, short z);
void   tf_map_setblock(TFMap* map, short x, short y, short z, byte bid);


#endif
