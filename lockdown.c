/*
 * TFbot by matthilde
 *
 * lockdown.c
 *
 * Freezes the map and prevent anyone from building.
 * Even though this is called "lockdown", this implements a block queue
 * that could be used for other applications as well.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "types.h"
#include "bot.h"

static void ld_delay(TFClient* client) {
    struct timespec delay = {
        client->state.builddelay / 1000,
        (client->state.builddelay & 1000) * 1000000
    };
    nanosleep(&delay, NULL);
}

void tf_lockdown_queue_add(TFBlockQueue *queue,
                           short x, short y, short z, byte bid) {
    TFBlock* block = (TFBlock*)xmalloc(sizeof(TFBlock));
    block->x = x; block->y = y; block->z = z; block->bid = bid;
    block->next = NULL;

    if (!(queue->tail)) queue->tail = block;
    if (queue->head) {
        queue->head->next = block;
        queue->head = queue->head->next;
    } else
        queue->head = block;
}
// NULL if there is nothing.
// the given block's "ownership" is passed to the user. They must
// free the block themself.
TFBlock* tf_lockdown_queue_remove(TFBlockQueue* queue) {
    if (!(queue->tail)) return NULL;

    TFBlock* result = queue->tail;
    queue->tail = queue->tail->next;
    if (!(queue->tail)) queue->head = NULL;

    return result;
}

void *tf_lockdown_job(void *c) {
    TFClient *client = (TFClient*)c;
    TFBlockQueue *queue = &(client->state.blockqueue);

    int *lockdownstop = &(client->state.lockdownstop);
    while (!(*lockdownstop)) {
        while (queue->tail && !*lockdownstop) {
            ld_delay(client);
            tf_job_lock(client);
            TFBlock *block = tf_lockdown_queue_remove(queue);
            tf_place_block(client->master,
                           block->x, block->y, block->z, block->bid);
            free(block);
            tf_job_unlock(client);
        }
        ld_delay(client);
    }

    while (queue->tail) free(tf_lockdown_queue_remove(queue));
    client->jobs[client->state.lockdownjobid] = 0;
    client->state.lockdownjobid = -1;

    CLIENTLOG(client, "Stopped lockdown successfully.\n");
    return NULL;
}
