/*
 * TFBot by matthilde
 *
 * bot.c
 *
 * This file contains more abstract commands for messaging, teleporting,
 * and other things like this. Also allows logging in a server.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>

#include "types.h"
#include "packet.h"
#include "bot.h"

void tf_message(int sock, const char* msg) {
    tf_send(sock, 0xd, 0, msg);
}

void tf_teleport(int sock, short x, short y, short z) {
    tf_send(sock, 0x8, rand() % 10, x, y, z, 0, 0);
}

void tf_place_block(int sock, short x, short y, short z, int bid) {
    tf_send(sock, 0x8, 255, x*32, y*32, z*32, 0, 0);
    tf_send(sock, 0x5, x, y, z, 1, bid);
}

int tf_login(const char* ip, int port,
             const char* name, const char* mppass,
             const char* clientname) {
    // Disable CPE if clientname is NULL
    int cpe = clientname != NULL;
    PacketArg argbuf[16];

    int sock = tf_connect_socket(ip, port);
    if (sock == -1) return -1;
    tf_send(sock, 0, 7, name, mppass, cpe * 0x42); // CPE!

    if (cpe) {
        int pid = tf_receive(sock, argbuf);
        if (pid == 0x10) {
            tf_send(sock, 0x10, clientname, 0);
        }
    }

    return sock;
}

//////////////////////////////////////////////////////////////////////

static void sanitized_strcpy(char* dest, const char* src) {
    char *ptr = dest;
    for (size_t i = 0; i < strlen(src); ++i) {
        if (src[i] == '&') ++i;
        else
            *(ptr++) = src[i];
    }
    *ptr = 0;
}

static void process_spawn_player(TFClient* client, PacketArg* argbuf) {
    byte playerid = argbuf[0].c.b;
    TFPlayer* player = &(client->players[playerid]);

    player->online = 1;
    sanitized_strcpy(player->name, argbuf[1].c.str);
    player->x = argbuf[2].c.s;
    player->y = argbuf[3].c.s;
    player->z = argbuf[4].c.s;
    player->yaw   = argbuf[5].c.b;
    player->pitch = argbuf[6].c.b;
}

static void process_despawn_player(TFClient* client, PacketArg* argbuf) {
    byte playerid = argbuf[0].c.b;
    TFPlayer* player = &(client->players[playerid]);

    player->online = 0;
}

static void process_move_player(TFClient* client, PacketArg* argbuf) {
    byte playerid = argbuf[0].c.b;
    TFPlayer* player = &(client->players[playerid]);
    
    player->x = argbuf[1].c.s;
    player->y = argbuf[2].c.s;
    player->z = argbuf[3].c.s;
    player->yaw   = argbuf[4].c.b;
    player->pitch = argbuf[5].c.b;
}

static void process_move_player_r(TFClient* client, PacketArg* argbuf,
                                  byte uprot) {
    byte playerid = argbuf[0].c.b;
    TFPlayer* player = &(client->players[playerid]);

    player->x += argbuf[1].c.sb;
    player->y += argbuf[2].c.sb;
    player->z += argbuf[3].c.sb;
    if (uprot) {
        player->yaw   = argbuf[4].c.b;
        player->pitch = argbuf[5].c.b;
    }
}
static void process_rotate_player(TFClient* client, PacketArg* argbuf) {
    byte playerid = argbuf[0].c.b;
    TFPlayer* player = &(client->players[playerid]);

    player->yaw   = argbuf[1].c.b;
    player->pitch = argbuf[2].c.b;
}

// #define TF_IGNOREMAPS
// receiving this with a lil bit more under the hood...
int tf_client_recv(TFClient* client, int* pid, PacketArg* argbuf) {
    if (client->master == -1) return 0;

    *pid = tf_receive(client->master, argbuf);
    // tf_print_packet(stderr, *pid, argbuf);
    if (*pid == -1) return -1;
    tf_job_lock(client);
    switch (*pid) {
    #ifndef TF_IGNOREMAPS
    case 0x02:
        if (client->state.ignoremap) break;
        tf_log(client->log, "Beginning map download...\n");
        tf_map_dlbegin(client->map); break;
    case 0x03:
        if (client->state.ignoremap) break;
        tf_map_dlchunk(client->map,
                              argbuf[1].c.arr,
                              argbuf[0].c.s);
        break;
    case 0x04:
        if (client->state.ignoremap) break;
        tf_log(client->log, "Finished downloading!\n");
        tf_map_dlfinish(client->map,
                               argbuf[0].c.s,
                               argbuf[1].c.s,
                               argbuf[2].c.s);
        break;
    case 0x06: 
        if (client->state.ignoremap) break;
        tf_map_setblock(client->map,
                        argbuf[0].c.s,
                        argbuf[1].c.s,
                        argbuf[2].c.s,
                        argbuf[3].c.b);
        break;
    #endif
    case 0x07: process_spawn_player(client, argbuf); break;
    case 0x08: process_move_player(client, argbuf); break;
    case 0x09:
        process_move_player_r(client, argbuf, 1); break;
    case 0x0a:
        process_move_player_r(client, argbuf, 0); break;
    case 0x0b:
        process_rotate_player(client, argbuf); break;
    case 0x0c: process_despawn_player(client, argbuf); break;
    case 0x0d:
        tf_log(client->chat, "%s\n", argbuf[1].c.str);
        break;
    default: break;
    }
    tf_job_unlock(client);

    return 1;
}

//////////////////////////////////////////////////////////////////////

// Returns -1 on failure, otherwise returns the ID of the job.
int tf_job_start(TFClient* client, void *(*routine)(void *)) {
    int error, id = -1;
    for (int i = 0; i < MAX_JOB_THREADS; ++i)
        if (client->jobs[i] == 0) { id = i; break; }
    if (id == -1) return -1;

    error = pthread_create(&(client->jobs[id]), NULL,
                           routine, client);
    if (error) pdie("tf_job_start: pthread_create");
    return id;
}
void tf_job_stop(TFClient* client, int id) {
    pthread_t pid = client->jobs[id];
    if (pid == 0) return;
    int error = pthread_cancel(pid);
    if (error) pdie("tf_job_stop: pthread_cancel");
    client->jobs[id] = 0;
}
void tf_job_stop_all(TFClient* client) {
    for (int i = 0; i < MAX_JOB_THREADS; ++i)
        tf_job_stop(client, i);
}

// We doing a little thread safety
void tf_job_lock(TFClient* client) {
    pthread_mutex_lock(&(client->lock));
}
void tf_job_unlock(TFClient* client) {
    pthread_mutex_unlock(&(client->lock));
}

//////////////////////////////////////////////////////////////////////

TFClient* tf_new_client(const char* name,
                        const char* clientname,
                        int slavecount) {
    TFClient* client = (TFClient*)calloc(sizeof(TFClient), 1);
    if (!client) die("tf_new_client: calloc");
    client->slaves = (int*)malloc(sizeof(int) * slavecount);
    if (!(client->slaves)) die("tf_new_client: malloc");
    client->slavecount = slavecount;

    client->map  = tf_new_map();
    client->log  = tf_new_log(BOT_LOG_SIZE);
    client->chat = tf_new_log(BOT_LOG_SIZE);

    strcpy(client->name, name);
    if (clientname) {
        client->state.cpe = 1;
        strcpy(client->clientname, clientname);
    } else
        client->state.cpe = 0;

    client->master = -1;
    for (int i = 0; i < slavecount; ++i)
        client->slaves[i] = -1;
    for (int i = 0; i < 256; ++i)
        client->players[i].online = 0;

    for (int i = 0; i < MAX_JOB_THREADS; ++i)
        client->jobs[i] = 0;
    int error = pthread_mutex_init(&(client->lock), NULL);
    if (error) pdie("tf_new_client: pthread_mutex_init");

    client->state.buildjobid = -1;
    client->state.lockdownjobid = -1;
    client->state.owner = -1;
    client->state.ignoremap = 0;

    return client;
}

void tf_free_client(TFClient* client) {
    tf_job_stop_all(client);
    pthread_mutex_destroy(&(client->lock));

    if (client->authenticated)
        tf_classicube_quit(&(client->cc));
    tf_free_map(client->map);
    tf_free_log(client->log);
    tf_free_log(client->chat);
    free(client->slaves);
    free(client);
}

// If -1, then failed.
int tf_client_connect_master(TFClient* client, const char* mppass,
                             const char *ip, int port) {
    client->master = tf_login(ip, port, client->name, mppass,
                              client->state.cpe ? client->clientname : NULL);
    strcpy(client->ip, ip);
    client->port = port;
    return client->master;
}

int tf_client_connect_slave(TFClient* client, int id, const char* mppass,
                              const char *ip, int port) {
    if (id >= client->slavecount || id < 0) return -1;
    if (client->slaves[id] != -1) return client->slaves[id];
    char namebuffer[strlen(client->name) + 4];
    sprintf(namebuffer, "%s%d", client->name, id);

    client->slaves[id] = tf_login(ip ,port, namebuffer, mppass,
                                  client->state.cpe ? client->clientname : NULL);

    return client->slaves[id];
}

void tf_client_disconnect_master(TFClient* client) {
    if (client->master != -1) {
        close(client->master);
        client->master = -1;
    }
}
void tf_client_disconnect_slave(TFClient* client, int id) {
    if (id >= 0 && id < client->slavecount
        && client->slaves[id] != -1) {
        close(client->slaves[id]);
        client->slaves[id] = -1;
    }
}
