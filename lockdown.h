#ifndef __TF_LOCKDOWN_H
#define __TF_LOCKDOWN_H

#include "types.h"

typedef struct TFBlock {
    short x, y, z; byte bid; struct TFBlock* next;
} TFBlock;

typedef struct TFBlockQueue {
    TFBlock *tail, *head;
} TFBlockQueue;

//// Functions
void*     tf_lockdown_job(void *c);
TFBlock*  tf_lockdown_queue_remove(TFBlockQueue* queue);
void      tf_lockdown_queue_add(TFBlockQueue* queue,
                                short x, short y, short z, byte bid);

#endif
