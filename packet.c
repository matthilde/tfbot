#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "types.h"
#include "packet.h"

#define TF_SPECIAL_VEC3     TF_FLOAT, TF_FLOAT, TF_FLOAT
#define TF_SPECIAL_UVCOORD  TF_SHORT, TF_SHORT, TF_SHORT
#define TF_SPECIAL_ANIMDATA TF_BYTE, TF_FLOAT, TF_FLOAT, TF_FLOAT, TF_FLOAT

/*************** TYPES ****************/
size_t tf_packet_sizes[] = {1, 1, 2, 64, 1024, 4, 256, 0};
PACKET_ARG_TYPES tf_packet_args[][PID_SIZE] = {
    {TF_BYTE, TF_STRING, TF_STRING, TF_BYTE, TF_END}, // 0x00 login
    {TF_END}, // 0x01 ping
    {TF_END}, // 0x02 level init
    {TF_SHORT, TF_BYTE_ARRAY, TF_BYTE, TF_END}, // 0x03 level data chunk
    {TF_SHORT, TF_SHORT, TF_SHORT, TF_END}, // 0x04 level finalize
    {TF_SHORT, TF_SHORT, TF_SHORT,
     TF_BYTE, TF_BYTE, TF_END}, // 0x05 setblock client
    {TF_SHORT, TF_SHORT, TF_SHORT, TF_BYTE, TF_END}, // 0x06 setblock server
    {TF_SBYTE, TF_STRING,
     TF_SHORT, TF_SHORT, TF_SHORT, TF_BYTE, TF_BYTE, TF_END}, // 0x07 spawn
    {TF_BYTE, TF_SHORT, TF_SHORT, TF_SHORT,
     TF_BYTE, TF_BYTE, TF_END}, // 0x08 position and orientation
    {TF_SBYTE, TF_SBYTE, TF_SBYTE, TF_SBYTE,
     TF_BYTE, TF_BYTE, TF_END}, // 0x09 pos ori update
    {TF_SBYTE, TF_SBYTE, TF_SBYTE, TF_SBYTE, TF_END}, // 0x0a pos update
    {TF_SBYTE, TF_BYTE, TF_BYTE, TF_END}, // 0x0b ori update
    {TF_SBYTE, TF_END}, // 0x0c despawn
    {TF_SBYTE, TF_STRING, TF_END}, // 0x0d message
    {TF_STRING, TF_END}, // 0x0e disconnect (kick or smth, or RAGEQUIT!!!)
    {TF_BYTE, TF_END}, // 0x0f update user type

    ///// CPE /////
    // Note that not all packets has been implemented.
    // They're defined so most of them can be ignored.
    {TF_STRING, TF_SHORT, TF_END}, // 0x10 ext_info
    {TF_STRING, TF_INT, TF_END}, // 0x11 ext_data

    {TF_SHORT, TF_END}, // 0x12 ClickDistance
    {TF_BYTE, TF_END},  // 0x13 CustomBlock - SupportLevel
    {TF_BYTE, TF_BYTE, TF_END}, // 0x14 HeldBlock - HoldThis
    {TF_STRING, TF_STRING, TF_INT, TF_BYTE, TF_END}, // 0x15 SetTextHotKey
    // 0x16 ExtAddPlayerName
    {TF_SHORT, TF_STRING, TF_STRING, TF_STRING, TF_BYTE, TF_END},
    {TF_END}, // 0x17 <nothing>
    {TF_SHORT, TF_END}, // 0x18 ExtRemovePlayerName
    {TF_BYTE, TF_SHORT, TF_SHORT, TF_SHORT, TF_END}, // 0x19 EnvSetColor
    // 0x1a MakeSelection
    {TF_BYTE, TF_STRING,
     TF_SHORT, TF_SHORT, TF_SHORT, TF_SHORT, TF_SHORT,
     TF_SHORT, TF_SHORT, TF_SHORT, TF_SHORT, TF_SHORT,
     TF_END },
    { TF_BYTE, TF_END }, // 0x1b RemoveSelection
    { TF_BYTE, TF_BYTE, TF_BYTE, TF_END }, // 0x1c SetBlockPermission
    { TF_BYTE, TF_STRING, TF_END }, // 0x1d ChangeModel
    { TF_END }, // 0x1e <nothing>
    { TF_BYTE, TF_END }, // 0x1f EnvWeatherType
    // 0x20 HackControl
    {TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_SHORT, TF_END},
    // 0x21 ExtAddEntity2
    {TF_BYTE, TF_STRING, TF_STRING, TF_SHORT, TF_SHORT, TF_SHORT,
     TF_BYTE, TF_BYTE, TF_END},
    // 0x22 PlayerClicked
    {TF_BYTE, TF_BYTE, TF_SHORT, TF_SHORT, TF_BYTE, TF_SHORT,
     TF_SHORT, TF_SHORT, TF_BYTE, TF_END},
    // 0x23 DefineBlock
    {TF_BYTE, TF_STRING,
     TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE,
     TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE,
     TF_END},
    {TF_BYTE, TF_END}, // 0x24 RemoveBlockDefinition
    // 0x25 DefineBlockExt
    {TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE,
     TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE,
     TF_END},
    // 0x26 BulkBlockUpdate
    {TF_BYTE, TF_BYTE_ARRAY, TF_BYTE_ARRAY256, TF_END},
    // 0x27 SetTextColor
    {TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_END},
    // 0x28 SetMapEnvUrl
    {TF_STRING, TF_END},
    // 0x29 SetMapEnvProperty
    {TF_BYTE, TF_INT, TF_END},
    // 0x2a SetEntityProperty
    {TF_BYTE, TF_BYTE, TF_INT, TF_END},
    // 0x2b TwoWayPing
    {TF_BYTE, TF_SHORT, TF_END},
    // 0x2c SetInventoryOrder
    {TF_BYTE, TF_BYTE, TF_END},
    // 0x2d SetHotbar
    {TF_BYTE, TF_BYTE, TF_END},
    // 0x2e SetSpawnpoint
    {TF_SHORT, TF_SHORT, TF_SHORT,
     TF_BYTE, TF_BYTE, TF_END},
    // 0x2f VelocityControl
    {TF_INT, TF_INT, TF_INT, TF_BYTE, TF_BYTE, TF_END},
    // 0x30 DefineEffect
    {TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE,
     TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE, TF_BYTE,
     TF_INT, TF_SHORT, TF_INT, TF_INT, TF_INT, TF_INT,
     TF_BYTE, TF_SBYTE, TF_END},
    // 0x31 SpawnEffect
    {TF_BYTE, TF_INT, TF_INT, TF_INT,
     TF_INT, TF_INT, TF_INT, TF_END},
    // 0x32 DefineModel
    {TF_BYTE, TF_STRING, TF_BYTE, TF_INT, TF_INT,
     TF_SPECIAL_VEC3, TF_SPECIAL_VEC3, TF_SPECIAL_VEC3,
     TF_SHORT, TF_SHORT, TF_BYTE, TF_END},
    // 0x33 DefineModelPart
    {TF_BYTE,
     TF_SPECIAL_VEC3, TF_SPECIAL_VEC3,
     TF_SPECIAL_UVCOORD, TF_SPECIAL_UVCOORD, TF_SPECIAL_UVCOORD,
     TF_SPECIAL_UVCOORD, TF_SPECIAL_UVCOORD, TF_SPECIAL_UVCOORD,
     TF_SPECIAL_VEC3, TF_SPECIAL_VEC3,
     TF_SPECIAL_ANIMDATA, TF_SPECIAL_ANIMDATA, TF_SPECIAL_ANIMDATA,
     TF_SPECIAL_VEC3, TF_BYTE, TF_END},
    // 0x34 UndefineModel
    {TF_BYTE, TF_END},
    { TF_END }, // 0x35 <nothing>
    // 0x36 ExtEntityTeleport
    {TF_BYTE, TF_BYTE, TF_SHORT, TF_SHORT, TF_SHORT,
     TF_BYTE, TF_BYTE, TF_END},
};

/*************** FUNCTIONS ****************/

void tf_parse_string(char* dest, const byte* src) {
    int i;
    for (i = 63; src[i] == ' ' && i > 0; --i);
    memcpy(dest, src, i + 1);
    dest[i + 1] = 0;
}

size_t tf_calculate_packet_size(PACKET_ARG_TYPES* args, size_t *psz) {
    size_t i, totalsize = 0;
    for (i = 0; args[i] != TF_END; ++i)
        totalsize += tf_packet_sizes[args[i]];
    if (psz) *psz = i;
    return totalsize;
}

char* tf_encode_packet(size_t *size, byte pid, va_list args) {
    if (pid > PID_SIZE) return NULL;
    PACKET_ARG_TYPES *packet_fmt = tf_packet_args[pid];
    size_t ptr = 1;
    size_t packet_size = tf_calculate_packet_size(packet_fmt, NULL);
    *size = packet_size + 1;
    char* buffer = (char*)xmalloc(sizeof(char) * (packet_size + 1));

    short short_tmp; char* str_tmp; size_t j;
    for (size_t i = 0; packet_fmt[i] != TF_END; ++i) {
        // checkpoint
        switch (packet_fmt[i]) {
        case TF_BYTE: case TF_SBYTE: // bytes
            buffer[ptr] = va_arg(args, int) & 0xff;
            break;
        case TF_SHORT: // shorts (big endian)
            short_tmp = va_arg(args, int) & 0xffff;
            buffer[ptr]   = (short_tmp >> 8) & 0xff;
            buffer[ptr+1] = short_tmp & 0xff;
            break;
        case TF_INT: // shorts (big endian)
            short_tmp = va_arg(args, int) & 0xffff;
            buffer[ptr]   = (short_tmp >> 24) & 0xff;
            buffer[ptr+1] = (short_tmp >> 16) & 0xff;
            buffer[ptr+2] = (short_tmp >> 8) & 0xff;
            buffer[ptr+3] = short_tmp & 0xff;
            break;
        case TF_STRING: // strings (with space padding)
            str_tmp = va_arg(args, char*);
            for (j = 0; j < 64 && str_tmp[j]; ++j)
                buffer[ptr+j] = str_tmp[j];
            for (; j < 64; ++j)
                buffer[ptr+j] = ' ';
            break;
        case TF_BYTE_ARRAY: // byte array
            str_tmp = va_arg(args, char*); // MUST be 1024 bytes long.
            for (j = 0; j < 1024; ++j) buffer[ptr+j] = str_tmp[j];
            break;
        case TF_BYTE_ARRAY256: // byte array
            str_tmp = va_arg(args, char*); // MUST be 256 bytes long.
            for (j = 0; j < 256; ++j) buffer[ptr+j] = str_tmp[j];
            break;
        default:
            die("Invalid argument type.");
        case TF_END:
            break;
        }
        ptr += tf_packet_sizes[packet_fmt[i]];
    }

    buffer[0] = pid;
    return buffer;
}

void tf_send(int sock, byte pid, ...) {
    if (pid > PID_SIZE) return;
    va_list args; size_t packet_sz; ssize_t socksz;

    va_start(args, pid);
    char* buffer = tf_encode_packet(&packet_sz, pid, args);
    va_end(args);

    socksz = send(sock, buffer, packet_sz, 0);
    if (socksz == -1) {
        if (errno == EPIPE) {

        } else {
            pdie("socket: send");
        }
    }

    free(buffer);
}

// Returns packet ID, -1 if fail or invalid.
int tf_receive(int sock, PacketArg* buffer) {
    byte buf[2048], pid; ssize_t ptr = 0, recvsize; size_t argsize;

    recvsize = recv(sock, &pid, 1, 0);
    if (recvsize == -1) pdie("socket: recv");
    if (recvsize == 0) return -1;
    if (pid > PID_SIZE) return -1;

    PACKET_ARG_TYPES *packet_fmt = tf_packet_args[pid];

    size_t packetsize = tf_calculate_packet_size(packet_fmt, &argsize);
    recvsize = recv(sock, buf, packetsize, MSG_WAITALL);
    if (recvsize == -1) pdie("socket: recv");
    if (packetsize != (size_t)recvsize) return -1;

    if (pid >= IGNORE_LIMIT) return -1; // IGNORED!!!!

    for (size_t i = 0; i < argsize; ++i) {
        buffer[i].type = packet_fmt[i];
        switch (packet_fmt[i]) {
        case TF_BYTE: case TF_SBYTE:
            buffer[i].c.b = buf[ptr]; break;
        case TF_SHORT:
            buffer[i].c.s = (buf[ptr]<<8) | buf[ptr+1]; // big endian
            break;
        case TF_INT:
            buffer[i].c.i  = buf[ptr]   << 24;
            buffer[i].c.i |= buf[ptr+1] << 16;
            buffer[i].c.i |= buf[ptr+2] << 8;
            buffer[i].c.i |= buf[ptr+3];
            break;
        case TF_STRING:
            // memcpy(buffer[i].c.str, &buf[ptr], 64);
            tf_parse_string(buffer[i].c.str, &buf[ptr]);
            break;
        case TF_BYTE_ARRAY:
            memcpy(buffer[i].c.arr, &buf[ptr], 1024);
            break;
        case TF_BYTE_ARRAY256:
            memcpy(buffer[i].c.arr, &buf[ptr], 256);
            break;
        default: break;
        }
        ptr += tf_packet_sizes[packet_fmt[i]];
    }
    buffer[argsize].type = TF_END;

    return pid;
}

////// MESSAGE STUFF
// returns 0 if it is not a message
static int string_is_valid_message(const char* s, int *loc) {
    for (const char* p = s; *p; ++p) {
        switch (*p) {
        case ' ': return 0;
        case ':':
            *loc = (int)(p - s);
            return 1;
        case '&': if (!*(++p)) return 0; break;
        }
    }
    return 0;
}
int tf_parse_message(const char* s, char* name, char* msg) {
    int semicolon;
    if (!string_is_valid_message(s, &semicolon)) return 0;

    int i, p = 0;
    char* curptr = name;
    for (i = 0; s[i]; ++i) {
        if (s[i] == '&') ++i;
        else if (i == semicolon) {
            curptr[p] = 0;
            p = 0; curptr = msg;
        }
        else
            curptr[p++] = s[i];
    }
    curptr[p] = 0;
    return 1;
}

// Connects to a server.
// I hate boilerplate code.
int tf_connect_socket(const char* addr, const int port) {
    struct sockaddr_in address;
    int sock;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return -1;

    address.sin_family = AF_INET;
    address.sin_port   = htons(port);

    if (inet_pton(AF_INET, addr, &address.sin_addr) <= 0)
        return -1;
    if (connect(sock, (struct sockaddr*)&address,
                sizeof(address)) == -1)
        return -1;

    return sock;
}

static void print_packet_arg(FILE* f, PacketArg* arg) {
    switch (arg->type) {
    case TF_BYTE:       fprintf(f, "%u", arg->c.b);   break;
    case TF_SBYTE:      fprintf(f, "%d", arg->c.sb);  break;
    case TF_SHORT:      fprintf(f, "%d", arg->c.s);   break;
    case TF_STRING:     fprintf(f, "\"\"%s\"\"", arg->c.str); break;
    case TF_BYTE_ARRAY: fprintf(f, "[DATA]");         break;
    default: break;
    }
}

void tf_print_packet(FILE* f, int pid, PacketArg* argbuf) {
    fprintf(f, "0x%02x:", pid);
    for (PacketArg* arg = argbuf; arg->type != TF_END; ++arg) {
        fputc(' ', f); print_packet_arg(f, arg);
    }
    fputc('\n', f);
}
