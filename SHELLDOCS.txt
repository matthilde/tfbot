TURBOFUCKER COMMAND-LINE SHELL DOCUMENTATION
============================================

The TurboFucker command-line shell plays an important role in the use of this
client because this will be how you're going to control it. I decided to use
the terminal instead of Minecraft Classic chat for the following reasons:

 - The ClassiCube chat feels clunky to use
 - More characters without having to implement LongerMessages
 - More privacy for passing sensitive informations
 - Makes it easy to implement front-ends by piping standard i/o
 - Because I prefer this way
 - yes

STARTUP AND SYNTAX
------------------

At start, the shell will run "turbofucker.cfg" located at your current working
directory. If there's not, it just won't run it. Configuration files uses the
same commands as the shell, so any commands mentionned here can be used in a
configuration file.

A command is typed like this:
    command arg1:arg2:arg3

The reason why arguments are seperated by : is because I was too lazy to write
a proper parser and instead preferred just using `strtok()`.

If your shell says "failure" after running a command, that means it failed to
run it, obviously.

COMMAND REFERENCE
-----------------

Here's now the complete reference of every command you can write in the shell

# comment
    This is a comment. Does nothing.

players
    Gives you a tab-seperated table of online players.
    In a row, first element is the player ID, second is the position,
    third is the name.

c <message>
    Sends something to the chat

scream <message>
    Every connected bot will send a message to the chat.

set-owner <id>
    Sets the owner to player ID. Fails if the player does not exist.
    Use `players` to find the appropriate ID.

    The owner will be primarily used by the `build` command where
    the bots will start building at the owner's position.

sphere <radius>:<bid>
    DEPRECATED, DO NOT USE.

    Builds a sphere of radius <radius> using the Block <bid> at the
    position of the owner.

    It is strongly advised to not use this command because it does not use the
    autobuild feature. You will likely be kicked for spamming.


load <filename>
    Loads a build in the Schematic or ClassicWorld format. Will fail to execute
    if the file is not valid, or if a build has already been loaded.

load-image <filename>
    Loads an image then turn it into a build. Will fail if the filename provided
    is not in a valid format.

build
    Build the loaded build at the position of the owner.

build-at <x>:<y>:<z>
    Build the loaded build at the given coordinates.

stop-build
    The bots will stop building if they have been currently building.

connect <ip>:<port>:<mppass>
    Connects to a server using the IP and port, authenticates using the
    specified mppass.

connect-slaves <mppass>
    Connects slaves with the given mppass to the server the master is currently
    on. Will fail if the master is not online.

viewlog <log>
    Views the contents of a log.

    if log is set to c, it will prints the 10 last lines of the chat.
    if log is set to l, it'll do the same with the system log.

funny
    May not work because only I can use this command.

set <key>:<value>
    Configure the client parameter <key> and set it to <value>.
    The parameters that can be configured are the following:

    name        Changes the name of the bot to use when connecting.
                default = tfbot
    builddelay  Changes the delay between building one block (in milliseconds)
                default = 30
    slavecount  Sets the slave count
                default = 48
    clientname  Sets the client name that will be shown when players type
                /clients
    
    The command will fail if this is not one of the followings.
    Note that the build delay has been set to the maximum according to the
    default MCGalaxy antispam limit. Trying to reduce the delay at this point
    will probably result in a kick.

cc-login <username>:<password>
    Connect to a ClassiCube account to be able to retrieve automatically
    the mppass for a server. If connection is successful, the parameter "name"
    will be changed to <username>.

cc-logoff
    Logs off ClassiCube.

cc-query <search>:<slient?>
    Searches through the server list. If <silent?> is set to t, it will not
    print out the results. 
    It will print an ID, the mppass, the server name and the ip. The ID can
    be used to join it.

cc-join <id>
    From the previous search, joins the server of the given ID.

    Example:
        tfbot> cc-query Server
        1   MPPASS  Epic Server!    127.0.0.1   25565
        2   MPPASS  Some Server.    192.168.1.1 25600
        tfbot> cc-join 1
        *will join Epic Server!*

TUTORIALS
=========

Here are some micro-tutorials to know how to do stuff
Things that starts with `tfbot>` are the commands you can type.
If a line starts with `--` that means it's explainations. Otherwise
it's the console output.

Setting up the turbofucker properly
-----------------------------------


 -- First set up everything you need
tfbot> set builddelay:30
tfbot> set name:MyAmazingBot
tfbot> set slavecount:4
 -- You can then connect to a server and connect your slaves if you want to
tfbot> connect 127.0.0.1:25565:mppass
tfbot> connect-slaves mppass
 -- You might want to checks the players with the `players` command and set
 -- your player as the owner for the `build` command.
tfbot> players
0   (26 155 38)     you
1   (1 92 23)       matthilde
255 (63 63 63)      MyAmazingBot
tfbot> set-owner 0
The owner is now you!

Connecting to a server with ClassiCube
--------------------------------------

 -- See the first tutorial to properly set up your bot but omit the connection
 -- part. Do these instead.

 -- First log in into your ClassiCube account.
tfbot> cc-login username:password
 -- It may fail due to ClassiCube's (wacky) 2FA. You can type `viewlog l` to
 -- see if it's a login failure. If you are sure you have typed the right creds,
 -- log in at https://classicube.net/ then try again.

 -- Once logged in, you can now look for servers.
tfbot> cc-query Classic:f
1   MPPASS Some Classic Server      127.0.0.1       25565
2   MPPASS Classic Anarchy Server   192.168.1.1     10000
 -- Say you want to join the Classic Anarchy Server, all you have to do is typing
tfbot> cc-join 2
 -- Your bot will automatically join the said server. connect-slaves does not work
 -- in this case, however.

Building something
------------------

 -- Building something is fairly simple. You just have to load the build build
 -- it.
tfbot> load myschematic.schematic
 -- This will load a schematic, you can however also load an image instead.
tfbot> load-image bnuuy.png
 -- To build it, you then have to type either build or buildat. Make sure to use
 -- the set-owner command for `build`.
tfbot> build
 -- This command will build your thing at X=64 Y=21 Z=13.
tfbot> build-at 64:21:13
 -- If you feel like you're not satisfied with your bot's art of work, you can
 -- still ask it to stop building.
tfbot> stop-build
