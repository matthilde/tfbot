/*
 * TFBot by matthilde
 *
 * schematic/nbt.c
 *
 * Basic API to read and dig through NBT data structures.
 * While it is quite limited, it has been implemented only to read
 * .schematic files.
 */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <zlib.h>

#include "../types.h"
#include "nbt.h"

#define TREEBUF_BASESIZE 512

typedef struct _bufstream { char* buf; size_t pos; } _bufstream;
typedef struct nbt_state {
    // Tree buffer
    TFNBT *nodes;
    size_t length;
    size_t used;

    // Buffer "stream"
    char* buf; size_t pos;
} nbt_state;

static void allocate_treebuf(nbt_state *tb, size_t len) {
    tb->length = len + 1;
    tb->nodes = (TFNBT*)xmalloc(sizeof(TFNBT) * tb->length);
    tb->used = 0;
    tb->nodes[0].type = NBT_UNUSED;
}
static TFNBT *nbt_new_node(nbt_state *tb) {
    TFNBT *node = &tb->nodes[tb->used++];
    tb->nodes[tb->used].type = NBT_UNUSED;
    node->name = NULL;
    node->length = -1;
    node->next   = NULL;
    return node;
}
static char* read_gzip_file(const char* filename, size_t* size) {
    gzFile f = gzopen(filename, "rb");
    if (!f) return NULL;
    if (gzbuffer(f, 1 << 24) < 0) return NULL;

    ssize_t buffersize = 1024;
    char* buffer = (char*)xmalloc(sizeof(char) * buffersize);
    ssize_t s = 0, totalsize = 0;

    while ((s = gzread(f, &buffer[totalsize], buffersize - totalsize)) > 0) {
        totalsize += s;
        if (totalsize >= buffersize) {
            buffersize *= 2;
            buffer = (char*)xrealloc(buffer,
                                     sizeof(char) * buffersize);
        }
    }
    if (s < 0) {
        free(buffer);
        return NULL;
    }

    *size = totalsize;
    gzclose(f);
    return buffer;
}

static void nbt_stream_skip(nbt_state* state, size_t len) {
    state->pos += len;
}
static void nbt_stream_read(nbt_state* state, void* buf,
                            size_t len) {
    memcpy((char*)buf, &(state->buf[state->pos]), len);
    state->pos += len;
}
inline static char nbt_stream_getc(nbt_state* state) {
    return state->buf[state->pos++];
}

#define LOOKAHEAD(n) (state->buf[state->pos + (n)] & 0xff)
#define ALLOC(t,n) (t*)xmalloc(sizeof(t) * (n))
inline static short nbt_stream_readshort(nbt_state* state) {
    unsigned short result = LOOKAHEAD(0) << 8 | LOOKAHEAD(1);
    state->pos += 2;
    return result;
}
inline static int nbt_stream_readint(nbt_state* state) {
    unsigned int result = LOOKAHEAD(3);
    result |= LOOKAHEAD(2) << 8;
    result |= LOOKAHEAD(1) << 16;
    result |= LOOKAHEAD(0) << 24;
    state->pos += 4;
    return result;
}
inline static long nbt_stream_readlong(nbt_state* state) {
    unsigned long result = (unsigned long)LOOKAHEAD(7);
    result |= (unsigned long)LOOKAHEAD(6) << 8;
    result |= (unsigned long)LOOKAHEAD(5) << 16;
    result |= (unsigned long)LOOKAHEAD(4) << 24;
    result |= (unsigned long)LOOKAHEAD(3) << 32;
    result |= (unsigned long)LOOKAHEAD(2) << 40;
    result |= (unsigned long)LOOKAHEAD(1) << 48;
    result |= (unsigned long)LOOKAHEAD(0) << 56;
    state->pos += 8;
    return result;
}
inline static char* nbt_stream_readstr(nbt_state* state, size_t *sz) {
    short len = nbt_stream_readshort(state);
    char* result = ALLOC(char, len + 1);
    nbt_stream_read(state, result, len);
    result[len] = 0;
    if (sz) *sz = len;
    return result;
}

//////////////////////////////////////////////////////////////////////

static TFNBT* nbt_parse_tag(nbt_state* state);
static TFNBT* nbt_decode_one_tag(nbt_state* state, int tag_id) {
    TFNBT *tag = nbt_new_node(state);
    size_t *length = &(tag->length); byte list_tagid;
    tag->type = tag_id;
    tag->c.l = 0;
    
    switch (tag_id) {
        // End
    case NBT_END:   break;
        // Number types
    case NBT_LONG:
    case NBT_DOUBLE:
        tag->c.l = nbt_stream_readlong(state);
        break;
    case NBT_INT:
    case NBT_FLOAT:
        tag->c.i = nbt_stream_readint(state);
        break;
    case NBT_SHORT:
        tag->c.i = nbt_stream_readshort(state);
        break;
    case NBT_BYTE:
        tag->c.b = nbt_stream_getc(state);
        break;
        // Array-like stuff
    case NBT_BYTE_ARRAY:
        *length = nbt_stream_readint(state);
        tag->c.barr = ALLOC(byte, *length);
        nbt_stream_read(state, tag->c.barr, *length);
        break;
    case NBT_INT_ARRAY:
        *length = nbt_stream_readint(state);
        tag->c.iarr = ALLOC(int, *length);
        for (size_t i = 0; i < *length; ++i)
            tag->c.iarr[i] = nbt_stream_readint(state);
        break;
    case NBT_LONG_ARRAY:
        *length = nbt_stream_readint(state);
        tag->c.larr = ALLOC(long, *length);
        for (size_t i = 0; i < *length; ++i)
            tag->c.larr[i] = nbt_stream_readlong(state);
        break;
    case NBT_STRING:
        tag->c.str = nbt_stream_readstr(state, length);
        break;
        // List-like stuff
    case NBT_LIST:
        list_tagid = nbt_stream_getc(state);
        *length = nbt_stream_readint(state);
        tag->c.list = ALLOC(TFNBT*, *length);
        for (size_t i = 0; i < *length; ++i)
            tag->c.list[i] = nbt_decode_one_tag(state, list_tagid);
        break;
    case NBT_COMPOUND:
        tag->c.nbt = nbt_parse_tag(state);
        TFNBT* curnode = tag->c.nbt;
        while (curnode->type != NBT_END) {
            curnode->next = nbt_parse_tag(state);
            if (!(curnode->next)) return NULL;
            curnode = curnode->next;
        }
        break;
    }

    return tag;
}

static TFNBT* nbt_parse_tag(nbt_state* state) {
    TFNBT *tag;
    byte tag_id = nbt_stream_getc(state);
    if (tag_id >= NBT_ENUM_LENGTH) {
        return NULL;
    } else if (tag_id == NBT_END) {
        tag = nbt_new_node(state);
        tag->type = NBT_END;
        return tag;
    }

    char* name = nbt_stream_readstr(state, NULL);
    tag        = nbt_decode_one_tag(state, tag_id);
    if (!tag) return NULL;
    tag->name = name;

    return tag;
}

static size_t nbt_count_nodes(nbt_state* state);
static size_t nbt_count_node(nbt_state* state, byte tag_id) {
    size_t length, count; byte list_tagid;
    switch (tag_id) {
        // End
    case NBT_END:   break;
        // Number types
    case NBT_LONG:
    case NBT_DOUBLE:
        nbt_stream_skip(state, 8);
        break;
    case NBT_INT:
    case NBT_FLOAT:
        nbt_stream_skip(state, 4);
        break;
    case NBT_SHORT:
        nbt_stream_skip(state, 2);
        break;
    case NBT_BYTE:
        nbt_stream_skip(state, 1);
        break;
        // Array-like stuff
    case NBT_BYTE_ARRAY:
        length = nbt_stream_readint(state);
        nbt_stream_skip(state, length);
        break;
    case NBT_INT_ARRAY:
        puts("!");
        length = nbt_stream_readint(state);
        nbt_stream_skip(state, length * 4);
        break;
    case NBT_LONG_ARRAY:
        length = nbt_stream_readint(state);
        nbt_stream_skip(state, length * 8);
        break;
    case NBT_STRING:
        length = nbt_stream_readshort(state);
        nbt_stream_skip(state, length);
        break;
        // List-like stuff
    case NBT_LIST:
        count = 1;
        list_tagid = nbt_stream_getc(state);
        length = nbt_stream_readint(state);
        for (size_t i = 0; i < length; ++i)
            count += nbt_count_node(state, list_tagid);
        return count;
    case NBT_COMPOUND:
        count = 1;
        size_t last_count = nbt_count_nodes(state);
        while (last_count != 0) {
            count += last_count;
            last_count = nbt_count_nodes(state);
        }
        count++; // The end node counts.
        return count;
        break;
    }
    return 1;
}

static size_t nbt_count_nodes(nbt_state* state) {
    size_t count = 0;

    byte tag_id = nbt_stream_getc(state);
    if (tag_id >= NBT_ENUM_LENGTH) {
        return 0;
    } else if (tag_id == NBT_END)
        return 0;

    short len = nbt_stream_readshort(state);
    nbt_stream_skip(state, len);
    count = nbt_count_node(state, tag_id);

    return count;
}

static void nbt_cleanup_node(TFNBT* node) {
    free(node->name);
    
    switch (node->type) {
    case NBT_BYTE_ARRAY:
    case NBT_INT_ARRAY:
    case NBT_LONG_ARRAY:
    case NBT_STRING:
    case NBT_LIST:
        free(node->c.ptr);
        break;
    default: break;
    }
}

// Must be the root node, otherwise it won't work
void tf_nbt_free(TFNBT* rootnode) {
    for (size_t i = 0; rootnode[i].type != NBT_UNUSED; ++i)
        nbt_cleanup_node(&rootnode[i]);
    free(rootnode);
}

TFNBT* tf_nbt_read_from_file(const char* filename) {
    size_t size;
    // Since it's used for schematics, it assumes the file is
    // gzip-compressed.
    char* buffer = read_gzip_file(filename, &size);
    if (!buffer) return NULL;
    
    nbt_state state;

    state.buf = buffer;
    state.pos = 0;
    size_t nodecount = nbt_count_nodes(&state);
    if (nodecount == 0) {
        free(buffer);
        return NULL;
    }
    allocate_treebuf(&state, nodecount);
    state.buf = buffer;
    state.pos = 0;

    TFNBT *root = nbt_parse_tag(&state);
    if (!root) {
        free(buffer);
        tf_nbt_free(state.nodes);
        return NULL;
    }

    assert(root == state.nodes);
    free(buffer);

    return root;
}

// returns NULL if not found.
TFNBT* tf_nbt_get_child(TFNBT* node, const char* name) {
    if (node->type != NBT_COMPOUND) return NULL;

    for (TFNBT* n = node->c.nbt; n->type != NBT_END; n = n->next)
        if (n->name && strcmp(n->name, name) == 0)
            return n;
    return NULL;
}

//////////////////////////////////////////////////////////////////////

#ifdef __TF_NBT_TEST

static char* _tag_types[] = {
    "End", "Byte", "Short", "Int", "Long", "Float", "Double",
    "ByteArray", "String", "List", "Compound",
    "IntArray", "LongArray"
};

static void print_result(TFNBT* node, int tab) {
    for (int i = 0; i < tab; ++i) putchar(' ');
    printf("%s('%s'): ",
           _tag_types[node->type],
           node->name ? node->name : "None");
    switch (node->type) {
    case NBT_END: puts("EndNode"); break;
    case NBT_BYTE:
    case NBT_SHORT:
    case NBT_INT:
        printf("%d", node->c.i);
        break;
    case NBT_LONG:
        printf("%ld", node->c.l);
        break;
    case NBT_FLOAT:
        printf("%f", node->c.f);
        break;
    case NBT_DOUBLE:
        printf("%lf", node->c.d);
        break;
    case NBT_STRING:
        printf("'%s'", node->c.str);
        break;
    case NBT_BYTE_ARRAY:
    case NBT_INT_ARRAY:
    case NBT_LONG_ARRAY:
        printf("[data length = %lu]", node->length);
        break;
    case NBT_LIST:
        printf("[\n");
        for (size_t i = 0; i < node->length; ++i)
            print_result(node->c.list[i], tab + 4);
        for (int i = 0; i < tab; ++i) putchar(' ');
        putchar(']');
        break;
    case NBT_COMPOUND:
        printf("{\n");
        for (TFNBT *n = node->c.nbt; n->type != NBT_END; n = n->next)
            print_result(n, tab + 4);
        for (int i = 0; i < tab; ++i) putchar(' ');
        putchar('}');
    default: break;
    }
    putchar('\n');
}

int main(int argc, char **argv) {

    TFNBT* root = tf_nbt_read_from_file("bigtest.nbt");
    if (!root) pdie("tf_nbt_read_from_file");

    print_result(root, 0);

    TFNBT *list = tf_nbt_get_child(root, "listTest (long)");
    assert(list != NULL);
    assert(list->type == NBT_LIST);
    assert(list->c.list[1]->c.l == 12L);

    tf_nbt_free(root);
    return 0;
}

#endif
