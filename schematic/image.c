/*
 * TFbot by matthilde
 *
 * schematic/image.c
 *
 * Generates a TFSchematic data structure based on an image.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "schematic.h"
#include "../types.h"

typedef struct __attribute__((__packed__)) color {
    byte r, g, b;
} color;

#define COLORS_AMOUNT 49
static color colors[] = {
    { 129, 129, 129 },
        { 123, 0, 0 },
    { 127, 92, 65 },
    { 128, 128, 128 },
    { 162, 131, 81 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 218, 211, 160 },
    { 141, 132, 132 },
    { 145, 144, 144 },
    { 138, 134, 131 },
    { 126, 126, 126 },
    { 111, 88, 54 },
    { 1, 0, 0 },
    { 192, 192, 65 },
    { 1, 0, 0 },
    { 255, 0, 0 },
    { 255, 150, 0 },
    { 255, 255, 0 },
    { 160, 255, 0 },
    { 0, 255, 0 },
    { 0, 255, 150 },
    { 0, 255, 255 },
    { 0, 150, 255 },
    { 0, 0, 255 },
    { 110, 0, 255 },
    { 180, 0, 255 },
    { 255, 0, 255 },
    { 255, 0, 160 },
    { 0, 0, 0 },
    { 142, 142, 142 },
    { 255, 255, 255 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 234, 181, 57 },
    { 197, 197, 197 },
    { 168, 168, 168 },
    { 1, 0, 0 },
    { 199, 127, 118 },
    { 154, 86, 78 },
    { 1, 0, 0 },
    { 110, 120, 110 },
    { 20, 18, 30 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 218, 210, 159 },
    { 1, 0, 0 },
    { 1, 0, 0 },
    { 208, 132, 152 },
    { 53, 70, 27 },
    { 79, 50, 31 },
    { 46, 57, 141 },
    { 46, 110, 136 },
    { 1, 0, 0 },
    { 214, 217, 236 },
    { 131, 42, 28 },
    { 230, 226, 218 },
    { 91, 70, 45 },
    { 123, 123, 123 },
};

static inline int image_pix_distance(color *c1, color *c2) {
    int r = c1->r - c2->r;
    int g = c1->g - c2->g;
    int b = c1->b - c2->b;
    return r*r + g*g + b*b;
}

static inline byte pix2block(color* c) {
    int mindist, min = 0, curdist;
    mindist = image_pix_distance(c, &colors[0]);
    for (int i = 1; i < COLORS_AMOUNT; ++i) {
        if (colors[i].r == 1) continue;

        if (mindist > (curdist = image_pix_distance(c, &colors[i]))) {
            min = i;
            mindist = curdist;
        }
    }

    return min + 1;
}

TFSchematic* tf_schematic_from_image(const char* filename) {
    int x, y, c;
    color *image = (color *)stbi_load(filename, &x, &y, &c, 3);
    if (!image) return NULL;

    TFSchematic *schem = (TFSchematic*)xmalloc(sizeof(TFSchematic));
    schem->x = x;
    schem->y = y;
    schem->z = 1;
    byte *blocks = (byte*)xmalloc(sizeof(byte) * (x * y));
    schem->buffer    = blocks;
    schem->bufferlen = x * y * 1;

    for (size_t i = 0; i < schem->bufferlen; ++i) {
        blocks[schem->bufferlen - 1 - i] = pix2block(&image[i]);
    }

    stbi_image_free(image);
    return schem;
}
