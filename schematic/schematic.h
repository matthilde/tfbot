#ifndef __TF_SCHEMATIC_SCHEMATIC_H
#define __TF_SCHEMATIC_SCHEMATIC_H

#include "../types.h"

typedef struct TFSchematic {
    byte *buffer; size_t bufferlen;
    int x, y, z; // size
} TFSchematic;

//// Functions
TFSchematic* tf_read_schematic(const char* filename);
void         tf_free_schematic(TFSchematic* schematic);
byte         tf_schematic_getblock(TFSchematic* schem,
                                   int x, int y, int z);

#endif
