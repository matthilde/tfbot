#ifndef __TF_SCHEMATIC_NBT_H
#define __TF_SCHEMATIC_NBT_H

#include "../types.h"

typedef enum {
    NBT_END,
    NBT_BYTE, NBT_SHORT, NBT_INT, NBT_LONG, NBT_FLOAT, NBT_DOUBLE,
    NBT_BYTE_ARRAY, NBT_STRING, NBT_LIST, NBT_COMPOUND,
    NBT_INT_ARRAY, NBT_LONG_ARRAY,

    NBT_ENUM_LENGTH, NBT_UNUSED
} TF_NBTType;

typedef struct TFNBT {
    char* name;
    TF_NBTType type;

    union {
        byte b;
        short s;
        int i;
        long l;
        float f;
        double d;
        byte* barr;
        int* iarr;
        long* larr;
        char* str;
        struct TFNBT* nbt;
        struct TFNBT** list;
        void* ptr;
    } c;

    size_t length;
    struct TFNBT *next;
} TFNBT;

//// Functions
void   tf_nbt_free(TFNBT* rootnode);
TFNBT* tf_nbt_read_from_file(const char* filename);
TFNBT* tf_nbt_get_child(TFNBT* node, const char* name);

#endif
