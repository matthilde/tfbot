/*
 * TFbot by matthilde
 *
 * schematic/schematic.c
 *
 * A set of functions to read Minecraft .schematic files.
 * It can also read ClassicWorld files and will automatically do so if
 * it detects it is one.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../types.h"
#include "nbt.h"
#include "schematic.h"

#define CHECK(c) if (!(c)) { tf_nbt_free(root); return NULL; }
#define GETCHILD(t,v,n) ({                      \
        CHECK(tmp = tf_nbt_get_child(root, n)); \
        v = (t)(tmp->c.l); })

static void schem_copy_map(byte* dest, int alpha, size_t len,
                          byte* blocks, byte* data) {
    static byte wool_replacements[16] = {
        15, 1, 11, 6, 2, 3, 12, 13, 14, 0, 9, 8, 0, 4, 0, 0,
    };
    if (alpha) {
        for (size_t i = 0; i < len; ++i) {
            if (blocks[i] == 35)
                dest[i] = 21 + wool_replacements[data[i]];
            else
                dest[i] = blocks[i];
        }
    } else
        memcpy(dest, blocks, len);
}

// returns NULL on error
TFSchematic* tf_read_schematic(const char* filename) {
    TFNBT *tmp;
    TFNBT *root = tf_nbt_read_from_file(filename);
    if (!root) return NULL;

    // This is where serious things begin...
    const char* materials;
    TFNBT *blocks, *data = NULL;
    int alpha;

    TFSchematic *schem = (TFSchematic*)xmalloc(sizeof(TFSchematic));

    if (strcmp(root->name, "Schematic") == 0) {
        GETCHILD(const char*, materials, "Materials");
        if (strcmp(materials, "Alpha") == 0) alpha = 1;
        else if (strcmp(materials, "Classic") == 0) alpha = 0;
        else { tf_nbt_free(root); return NULL; }

        GETCHILD(int, schem->x, "Width");
        GETCHILD(int, schem->y, "Height");
        GETCHILD(int, schem->z, "Length");
        CHECK(blocks = tf_nbt_get_child(root, "Blocks"));
        if (alpha)
            CHECK(data   = tf_nbt_get_child(root, "Data"));
    } else if (strcmp(root->name, "ClassicWorld") == 0) {
        alpha = 0;
        GETCHILD(short, schem->x, "X");
        GETCHILD(short, schem->y, "Y");
        GETCHILD(short, schem->z, "Z");
        CHECK(blocks = tf_nbt_get_child(root, "BlockArray"));
    } else
        CHECK(0);

    schem->buffer    = (byte*)xmalloc(sizeof(byte) * blocks->length);
    schem->bufferlen = blocks->length;
    schem_copy_map(schem->buffer, alpha, schem->bufferlen,
                   blocks->c.barr, data ? data->c.barr : NULL);

    tf_nbt_free(root);
    return schem;
}

void tf_free_schematic(TFSchematic* schematic) {
    free(schematic->buffer);
    free(schematic);
}

byte tf_schematic_getblock(TFSchematic* schem, int x, int y, int z) {
    if (x >= schem->x || y >= schem->y || z >= schem->z ||
        x < 0 || y < 0 || z < 0)
        return 0;
    size_t idx = (y * schem->z + z) * schem->x + x;
    return schem->buffer[idx];
}

#ifdef __TF_SCHEMATIC_TEST

int main(int argc, const char **argv) {
    TFSchematic *schematic = tf_read_schematic(argv[1]);

    if (schematic)
        tf_free_schematic(schematic);
    else
        puts("Failed to load schematic file!");
    return 0;
}

#endif
