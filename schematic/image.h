#ifndef __TF_SCHEMATIC_IMAGE_H
#define __TF_SCHEMATIC_IMAGE_H

TFSchematic* tf_schematic_from_image(const char* filename);

#endif
