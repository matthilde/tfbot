#ifndef __TF_PACKET_H
#define __TF_PACKET_H

#include <stddef.h>
#include <stdarg.h>
#include "types.h"

size_t tf_calculate_packet_size(PACKET_ARG_TYPES* args, size_t *psz);
char* tf_encode_packet(size_t* size, byte pid, va_list args);

void tf_send(int sock, byte pid, ...);
int  tf_receive(int sock, PacketArg* buffer);

int tf_parse_message(const char* s, char* name, char* msg);

int tf_connect_socket(const char* addr, const int port);
void tf_print_packet(FILE* f, int pid, PacketArg* argbuf);


#endif
