/*
 * TFBot by matthilde
 *
 * map.c
 *
 * Downloads and manages maps.
 */
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include <time.h>

#include "map.h"
#include "types.h"

TFMap* tf_new_map() {
    TFMap* map = (TFMap*)malloc(sizeof(TFMap));
    if (!map) die("tf_new_map: malloc");
    
    map->buffer = NULL;
    map->downloading = 0;

    return map;
}

void tf_free_map(TFMap* map) {
    if (map->buffer) free(map->buffer);
    free(map);
}

void tf_map_dlbegin(TFMap* map) {
    if (map->downloading) return;
    if (map->buffer) free(map->buffer);
    map->downloading = 1;

    sprintf(map->tmpfn, "/tmp/tfbot-%lu.gz", time(NULL));
    FILE* f = fopen(map->tmpfn, "wb");
    if (!f) die("tf_map_dlbegin: fopen");
    map->tempfile = f;
}

void tf_map_dlchunk(TFMap* map, byte* chunk, size_t size) {
    if (!(map->downloading)) return;
    fwrite(chunk, 1, size, map->tempfile);
}

void tf_map_dlfinish(TFMap* map, short x, short y, short z) {
    if (!(map->downloading)) return;
    fclose(map->tempfile);

    map->downloading = 0;
    map->x = x; map->y = y; map->z = z;
    map->buflen = x * y * z + 4;
    map->buffer = (byte*)xmalloc(sizeof(byte) * map->buflen);

    gzFile compressed = gzopen(map->tmpfn, "rb");
    ssize_t readsize = gzread(compressed, map->buffer, map->buflen);
    (void)readsize;

    gzclose(compressed);
    remove(map->tmpfn);
}

static inline size_t GETIDX(TFMap* map, size_t x, size_t y, size_t z) {
    return (x+(z*map->x)+(y*map->x*map->z) + 4);
}
byte tf_map_getblock(TFMap* map, short x, short y, short z) {
    size_t idx = GETIDX(map,x,y,z);
    if (idx >= map->buflen) return 0;
    return map->buffer[idx];
}
void tf_map_setblock(TFMap* map, short x, short y, short z, byte bid) {
    size_t idx = GETIDX(map,x,y,z);
    if (idx > map->buflen) return;
    map->buffer[idx] = bid;
}
